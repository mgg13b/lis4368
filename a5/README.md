> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Development

## Mitchell Gayne

### Assignment 4 Requirements:

## Developpment Installation:

1. Compile Java programs processing information
2. Use mySQL to to take information from the jsp.
3. Validate the server to keep the input to the server correct.
			


> #### Git commands w/short descriptions:

1. git init - definition goes here... 
2. git status - initializes a git repository
3. git add - adds file changes in your working directory to your index.
4. git commit - Takes all of the changes written in the index
5. git push - Pushes the files from the remote repository and merges it with your local one.
6. git pull - Fetches the files from the remote repository that are not present in the local one.
7. git remote - shows all the remote versions of your repository.

#### Project Screenshots:


*Blank fields showing failure*:

![Failure Data Validation](img/success.png)


*Filled fields showing succes*:

![Successful Data Validation](img/thanks.png)

*SQL Confirmation

![SQL Input](img/sql.png)
