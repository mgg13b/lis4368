#LIS 4368

##Mitchell Gayne

### Assignment # Requirements:

[A1 Subdirectory](a1/README.md "A1 Coursework")

1. Screenshot of running java Hello
2. Screen Shot of http//localhost:999
3. git commands with short descriptions
4. Bitbucket repo links a) this assignment b) the completed tutorial above
5. Chapter Questions

[A2 Subdirectory](a2/README.md "A1 Coursework")

1. Download and install MySQL:
2. Create a New User, inclusive.
3. Develop and Deploy a WebApp
4. Deploying Servlet using @WebServlet (Servlet 3.0 on Tomcat 7),
inclusive. (Note: we will be using the @WebServlet method for the remaining assignments.)

[A3 Subdirectory](a3/README.md "A3 Coursework")

1. Create a database ERD 
2. Forward engineer database with 3 tables
3. Create a SQL script of the forward engineered table so that it can be remade later.


[P1 Subdirectory](p1/README.md "P1 Coursework")

1. Suitably modify meta tags
2. Change title, navigation links, and header tags appropriately
3. Add form controls to match attributes of customer entity
4. Add the following jQuery validation and regular expressions-- as per the entity
attribute requirements (and screenshots below):
	1. *All* input fields, except Notes are required
	2. Use min/max jQuery validation
	3. Use regexp to only allow appropriate characters for each control:
	
[A4 Subdirectory](a4/README.md "A4 Coursework")

1. Model: - The “business” layer. how do we represent the data? Defines business objects, and provides
methods for business processing (e.g., Customer and CustomerDB classes).
2. Controller: Directs the application “flow”—using servlets. Generally, reads parameters sent from
requests, updates the model, saves data to the data store, and forwards updated model to JSPs for
presentation (e.g., CustomerListServlet).
3. View: How do I render the result? Defines the presentation—using .html or .jsp files (e.g., index.html,
index.jsp, thanks.jsp).



[A5 Subdirectory](a5/README.md "A5 Coursework")

1. Compile Java programs processing information
2. Use mySQL to to take information from the jsp.
3. Validate the server to keep the input to the server correct.






[P2 Subdirectory](p2/README.md "P2 Coursework")

1. These requirements complete the JSP/Servlets web application using the MVC framework and providing
create, read, update and delete (CRUD) functionality--that is.
2. the four basic functions of persistent
storage. 
3. In addition a search (SCRUD) option could be added, and which will be left as a research
exercise.




