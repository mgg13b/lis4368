> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Development

## Mitchell Gayne

### Assignment 2 Requirements:

## Developpment Installation:

1. Download and install MySQL:
2. Create a New User, inclusive.
3. Develop and Deploy a WebApp
4. Deploying Servlet using @WebServlet (Servlet 3.0 on Tomcat 7),
inclusive. (Note: we will be using the @WebServlet method for the remaining assignments.)


> #### Git commands w/short descriptions:

1. git init - definition goes here... 
2. git status - initializes a git repository
3. git add - adds file changes in your working directory to your index.
4. git commit - Takes all of the changes written in the index
5. git push - Pushes the files from the remote repository and merges it with your local one.
6. git pull - Fetches the files from the remote repository that are not present in the local one.
7. git remote - shows all the remote versions of your repository.

#### Assignment Screenshots:

*Screenshot of running Hello*:

![http://localhost:9999/hello](img/initial_hello.png)

*Screenshot of running index*:

![http://localhost:9999/hello/index.html](img/hello_index.png)

*Screenshot of running sayhello*:

![http://localhost:9999/hello/sayhello](img/sayhello.png)

*Screenshot of running java querybook*:

![http://localhost:9999/hello/querybook.html](img/querybook.png)

*Screenshot of running java say hi*:

![http://localhost:9999/hello/sayhi](img/sayhi.png)

*Screenshot of running java perform query*:

![http://localhost:9999/hello/querybook.html](img/performquery.png)

*Screenshot of running java query result*:

![Query Results](img/queryresult.png)