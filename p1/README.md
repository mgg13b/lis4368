> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Development

## Mitchell Gayne

### Project 1 Requirements:

## Developpment Installation:

1. Suitably modify meta tags
2. Change title, navigation links, and header tags appropriately
3. Add form controls to match attributes of customer entity
4. Add the following jQuery validation and regular expressions-- as per the entity
attribute requirements (and screenshots below):
	1. *All* input fields, except Notes are required
	2. Use min/max jQuery validation
	3. Use regexp to only allow appropriate characters for each control:
			


> #### Git commands w/short descriptions:

1. git init - definition goes here... 
2. git status - initializes a git repository
3. git add - adds file changes in your working directory to your index.
4. git commit - Takes all of the changes written in the index
5. git push - Pushes the files from the remote repository and merges it with your local one.
6. git pull - Fetches the files from the remote repository that are not present in the local one.
7. git remote - shows all the remote versions of your repository.

#### Project Screenshots:


*Blank fields showing failure*:

![Failure Data Validation](img/fail.png)


*Filled fields showing succes*:

![Successful Data Validation](img/success.png)


