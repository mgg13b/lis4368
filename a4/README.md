> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Development

## Mitchell Gayne

### Assignment 4 Requirements:

## Developpment Installation:

1. Model: - The “business” layer. how do we represent the data? Defines business objects, and provides
methods for business processing (e.g., Customer and CustomerDB classes).
2. Controller: Directs the application “flow”—using servlets. Generally, reads parameters sent from
requests, updates the model, saves data to the data store, and forwards updated model to JSPs for
presentation (e.g., CustomerListServlet).
3. View: How do I render the result? Defines the presentation—using .html or .jsp files (e.g., index.html,
index.jsp, thanks.jsp).

			


> #### Git commands w/short descriptions:

1. git init - definition goes here... 
2. git status - initializes a git repository
3. git add - adds file changes in your working directory to your index.
4. git commit - Takes all of the changes written in the index
5. git push - Pushes the files from the remote repository and merges it with your local one.
6. git pull - Fetches the files from the remote repository that are not present in the local one.
7. git remote - shows all the remote versions of your repository.

#### Project Screenshots:


*Blank fields showing failure*:

![Failure Data Validation](img/fail.png)


*Filled fields showing succes*:

![Successful Data Validation](img/success.png)
