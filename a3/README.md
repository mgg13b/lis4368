> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Development

## Mitchell Gayne

### Assignment 3 Requirements:

## Developpment Installation:

1. Create a ERD for a theoretical petstore
2. Forward engineer database with 3 tables 
3. Create a SQL script of the forward engineered table so that it can be remade later.


> #### Git commands w/short descriptions:

1. git init - definition goes here... 
2. git status - initializes a git repository
3. git add - adds file changes in your working directory to your index.
4. git commit - Takes all of the changes written in the index
5. git push - Pushes the files from the remote repository and merges it with your local one.
6. git pull - Fetches the files from the remote repository that are not present in the local one.
7. git remote - shows all the remote versions of your repository.

#### Assignment Screenshots:

*Screenshot of ERD*:

![Screenshot of ERD](img/ERD.png)

*Link to a3.mwb*:

[link to a3.mwb](docs/a3.mwb)

*link to a3.sql*:

[Link to a3.sql](docs/a3.sql)


