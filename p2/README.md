> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Development

## Mitchell Gayne

### Project 1 Requirements:

## Developpment Installation:


1. These requirements complete the JSP/Servlets web application using the MVC framework and providing
create, read, update and delete (CRUD) functionality--that is.
2. the four basic functions of persistent
storage. 
3. In addition a search (SCRUD) option could be added, and which will be left as a research
exercise.
			


> #### Git commands w/short descriptions:

1. git init - definition goes here... 
2. git status - initializes a git repository
3. git add - adds file changes in your working directory to your index.
4. git commit - Takes all of the changes written in the index
5. git push - Pushes the files from the remote repository and merges it with your local one.
6. git pull - Fetches the files from the remote repository that are not present in the local one.
7. git remote - shows all the remote versions of your repository.

#### Project Screenshots:


*Blank fields showing failure*:

![Failure Data Validation](img/one.png)


*Filled fields showing succes*:

![Successful Data Validation](img/two.png)

*Filled fields showing succes*:

![Successful Data Validation](img/three.png)


*Filled fields showing succes*:

![Successful Data Validation](img/four.png)


*Filled fields showing succes*:

![Successful Data Validation](img/five.png)


*Filled fields showing succes*:

![Successful Data Validation](img/six.png)


*Filled fields showing succes*:

![Successful Data Validation](img/seven.png)





