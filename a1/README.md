> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Development

## Mitchell Gayne

### Assignment 1 Requirements:

*Three Parts:*

1.Distributed Version Control with Git and Bitbucket
2. Java/JSP/Servlet Development Installation
3. Chapter Questions (Chs 1-4)

#### README.md file should include the following items:

* Bullet-list items
* Screenshot of running java Hello
* Screenshot of running http://localhost:9999
* git commands w/ short descriptions;
* Bitbucket repo links: a) this assignment and b) the completed tutorial above (bitbucketstationlocations).

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - definition goes here... 
2. git status - initializes a git repository
3. git add - adds file changes in your working directory to your index.
4. git commit - Takes all of the changes written in the index
5. git push - Pushes the files from the remote repository and merges it with your local one.
6. git pull - Fetches the files from the remote repository that are not present in the local one.
7. git remote - shows all the remote versions of your repository.

#### Assignment Screenshots:

*Screenshot of Tomcat running http://localhost*:

![Tomcat Installation Screenshot](img/tomcat.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/java.png)



#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/mgg13b/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/mgg13b/myteamquotes/ "My Team Quotes Tutorial")
